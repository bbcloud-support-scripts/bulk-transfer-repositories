from requests import Session

try:
    import env
    username = env.username
    password = env.password
    old_workspace = env.old_workspace
    new_workspace = env.new_workspace
    csrftoken = env.csrftoken
    cookies = env.cookies

    session = Session()
    session.auth = (username, password)
    repo_url = f"https://api.bitbucket.org/2.0/repositories/{old_workspace}"
    transfer_url = f"https://bitbucket.org/{old_workspace}/"
except [ImportError, AttributeError]:
    print('Could not locate one or more attributes within the "env.py" file. '
          'Please follow the readme to ensure that all attributes are present and try again.')
    exit()  

def get_repos(page=None, limit=1_000):
    while True:
        params = {'page': page, 'limit': limit}
        endpoint = f'{repo_url}'
        r = session.get(url=endpoint, params=params)
        r_json = r.json()
        for raw_repo in r_json.get('values'):
            yield raw_repo.get('slug')

        if page == None:
            page = 1

        if r_json.get('next'):
            page += 1
        else:
            return

def transferRepo(repo_name):
    data = {'user': new_workspace}
    endpoint = transfer_url + repo_name + '/admin/transfer'
    headers = {
        'Referer': endpoint,
        'X-CSRFToken': csrftoken,
        'cookie': cookies,
        'Accept': '*/*',
        'Accept-Language': 'en-US,en;q=0.7,fr-FR;q=0.3',
        'Accept-Encoding': 'gzip, deflate, br',
        'X-Requested-With': 'XMLHttpRequest',
        'X-Bitbucket-Frontend': 'frontbucket',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'https://bitbucket.org',
        'Connection': 'keep-alive',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin'
    }
    r = session.post(endpoint, headers=headers, data=data)
    return r.status_code, r.text

def main():
    for repo_slug in get_repos():
        status_code, response_text = transferRepo(repo_slug)
        
        if status_code == 200:
            print(f"Transfer request intiated for the repo {repo_slug}")
        else:
            print(str(status_code) + ':' + str(response_text) + ' - ' + repo_slug)
            
            
if __name__ == '__main__':
    main()
