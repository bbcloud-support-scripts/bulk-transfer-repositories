username      = '' # Cloud admin username
password      = '' # Cloud app password
old_workspace = '' # Cloud workspace slug/id for the current workspace (as seen in the url)
new_workspace = '' # Cloud workspace slug/id for the new workspace (as seen in the url)
csrftoken     = '' # Check the Readme file for further instructions on how to get this
cookies       = '' # Check the Readme file for further instructions on how to get this
