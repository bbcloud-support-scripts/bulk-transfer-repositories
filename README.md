## Disclaimer
This tool was NOT written by Atlassian developers and is considered a third-party tool. This means that this is also NOT supported by Atlassian. We highly recommend you have your team review the script before running it to ensure you understand the steps and actions taking place, as Atlassian is not responsible for the resulting configuration.

## Purpose
This script will automatically initiate a transfer for all repositories in a given workspace to a new workspace, reducing the effort of having to navigate repo by repo in order to transfer them on migration scenarios.

## How to Use
Edit rename/copy the "env-template.py" file to "env.py" (as env.py is in the .gitignore) and fill it out accordingly. In order to get the `crsftoken` and the `cookies`, you will need to:
        
1. Through the UI, navigate to a repository setting and open the option to transfer the repo;
2. Fill in the destination workspace;
3. Open the browser dev tools in the network tab;
4. As soon as you click **transfer**, you should see a POST request to the following URL `https://bitbucket.org/<workspace>/<repository>/admin/transfer`;
5. Open that request and under the **Request headers** section, grab the full content     of **x-csrftoken** and **cookies** and store them inside the single quotes in the env.py variables.

Install package dependencies with the follow commands:

        $ pip3 install -r requirements.txt

Once the dependencies are satisfied and you have provided your unique details, simply run the script with Python 3.6+ and follow any prompts.

Run script with python via:

        $ python3 bulk_transfer_repos.py

